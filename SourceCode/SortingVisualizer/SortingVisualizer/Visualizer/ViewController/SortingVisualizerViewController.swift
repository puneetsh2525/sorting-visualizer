import UIKit

class SortingVisualizerViewController: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var sortBarButtonItem: UIBarButtonItem!
    
    // MARK: - UICollectionViewDiffableDataSource
    
    private lazy var datasource: DataSource = makeDataSource()
    
    private var headerViews: [IndexPath: SectionHeaderView] = [:]
    
    // MARK: - ViewModel
    
    private lazy var viewModel = SortingVisualizerViewModel(
        collectionViewWidth: collectionView.bounds.size.width,
        datasource: datasource
    )
    
    // MARK: - ViewLifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Sorting Visualizer"

        setupCollectionView()
        sortBarButtonItem.title = viewModel.barButtonTitle
        
        viewModel.updateHeaderView = { [weak self] indexPath in
            guard let self = self else { return }

            let headerView = self.headerViews[indexPath]!
            
            self.update(headerView: headerView, atIndexPath: indexPath)
        }
    }
}

// MARK: - IBActions

private extension SortingVisualizerViewController {
    
    @IBAction
    func sort() {
        viewModel.isSorted = !viewModel.isSorted
        sortBarButtonItem.title = viewModel.barButtonTitle
    }
}

// MARK: - CollectionView

private extension SortingVisualizerViewController {
    
    func setupCollectionView() {
        collectionView.dataSource = datasource
        
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = viewModel.itemSize
    }
    
    func makeDataSource() -> DataSource {
        let reuseIdentifier = "ItemCell"
        
        let datasource = DataSource(
            collectionView: collectionView,
            cellProvider: {  collectionView, indexPath, item in
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ItemCell

                cell.textLabel.text = item.description
                
                return cell
            }
        )
        
        datasource.supplementaryViewProvider = { collectionView, kind, indexPath in
            
            let headerView = collectionView.dequeueReusableSupplementaryView(
            ofKind: kind,
            withReuseIdentifier: "SectionHeaderView",
            for: indexPath
            ) as! SectionHeaderView
            
            self.headerViews[indexPath] = headerView
            
            self.update(headerView: headerView, atIndexPath: indexPath)
            
            return headerView
        }
    
        return datasource
    }
    
    private func update(headerView: SectionHeaderView, atIndexPath indexPath: IndexPath) {
        let section = self.viewModel.section(atIndex: indexPath.section)
        
        headerView.textLabel.text = section?.name
        headerView.totalStepsCountLabel.text = section?.totalSteps.description
    }
}
