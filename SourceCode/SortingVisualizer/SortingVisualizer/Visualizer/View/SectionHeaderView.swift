import UIKit

class SectionHeaderView: UICollectionReusableView {
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var totalStepsCountLabel: UILabel!
}
