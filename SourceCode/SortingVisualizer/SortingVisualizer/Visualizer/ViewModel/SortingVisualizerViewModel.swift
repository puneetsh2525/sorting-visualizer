import UIKit

typealias RowItem = Item<Int>
typealias SectionItem = Section<RowItem>
typealias DataSource = UICollectionViewDiffableDataSource<SectionItem, RowItem>
typealias DataSourceSnapshot = NSDiffableDataSourceSnapshot<SectionItem, RowItem>

class SortingVisualizerViewModel {
    
    // MARK: - Bar Button Title
    
    var barButtonTitle: String {
        return isSorted ? "Reset" : "Sort"
    }
    
    // MARK: - Is Sorted
    
    var isSorted: Bool = false {
        didSet {
            switch isSorted {
            case true:
                sections.enumerated().forEach { index, section in
                    sortNext(for: section, atIndex: index)
                }
                
            case false:
                sections = newSections()
            }
        }
    }
    
    var updateHeaderView: ((IndexPath) -> Void)?
    
    // MARK: - Item Size
    
    private(set) lazy var itemSize: CGSize = CGSize(width: 40, height: 40)
    
    // MARK: - Sections
    
    private var sections: [SectionItem] = [] {
        didSet {
            resetDatasource()
        }
    }
    
    // MARK: - Private Properties
    
    private let collectionViewWidth: CGFloat
    private let dataSource: DataSource
    
    // MARK: - Init
    
    init(collectionViewWidth: CGFloat,
         datasource: DataSource) {
        self.collectionViewWidth = collectionViewWidth
        self.dataSource = datasource
        
        sections = newSections()
        resetDatasource()
    }
    
    // MARK: - section(atIndex:)
    
    func section(atIndex index: Int) -> SectionItem? {
        guard index < sections.count else {
            return nil
        }
        
        return sections[index]
    }
}

// MARK: - Private

private extension SortingVisualizerViewModel {
    
    func newSections() -> [SectionItem] {
        
        func items() -> [RowItem] {
            let numberOfItems = Int(collectionViewWidth / itemSize.width) + 5
            
            var array: [RowItem] = []
            
            for index in 1...numberOfItems {
                array.append(Item(value: index))
            }
            
            return array.shuffled()
        }
        
        let shuffledItems = items()
        
        return [
            Section(items: shuffledItems, sortingType: .insertionSort),
            Section(items: shuffledItems.map { RowItem(value: $0.value) }, sortingType: .quickSort)
        ]
    }
    
    func resetDatasource() {
        DispatchQueue.main.async {
            var snapshot = DataSourceSnapshot()
            snapshot.deleteAllItems()
            snapshot.appendSections(self.sections)
            
            self.sections.forEach {
                snapshot.appendItems($0.items, toSection: $0)
            }
            
            self.dataSource.apply(snapshot)
        }
    }
    
    func sortNext(for section: SectionItem, atIndex index: Int) {
        section.sortNext {
            guard section.isSorted == false else {
                return
            }
            
            var currentSnapshot = dataSource.snapshot()
            currentSnapshot.deleteItems(section.items)
            currentSnapshot.appendItems(section.items, toSection: section)
            
            updateHeaderView?(IndexPath(row: 0, section: index))
            
            dataSource.apply(currentSnapshot) {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.09) { [weak section] in
                    guard let section = section else { return }
                    self.sortNext(for: section, atIndex: index)
                }
            }
        }
    }
}
