import Foundation

enum SortingAlgorithmError: Error {
    case isSorted
}

class SortingAlgorithm<T: Comparable> {
    required init(items: [T]) {}
    
    typealias SuccessResult = (items: [T], steps: Int)
    
    func next() -> Result<SuccessResult, SortingAlgorithmError> {
        fatalError("should be subclassed")
    }
    
    var isSorted: Bool { return false }
    
    typealias Item = T
}
