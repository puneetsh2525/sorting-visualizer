import Foundation

class QuickSort<T: Comparable>: SortingAlgorithm<T> {
    typealias Item = T
    
    // MARK: - Items
    
    private(set) var items: [Item]
    
    // MARK: - Variables
    
    private var totalSteps = 1
    
    private lazy var stack = Stack<Int>()
    
    private var low: Int = 0
    
    private var high: Int = 0

    var pivotIndex: Int = -1
    
    private lazy var i = low
    
    private lazy var j = low

    // MARK: - API
     
    override var isSorted: Bool {
        low > high
    }
    
    required init(items: [T]) {
        self.items = items
        
        super.init(items: items)
        
        if items.count > 1 {
            stack.push(0)
            stack.push(items.count - 1)
        }
    }
    
    override func next() -> Result<SuccessResult, SortingAlgorithmError> {
        defer {
            totalSteps += 1
        }
        
        if pivotIndex == -1 {
            while stack.isEmpty == false && high - low < 1 {
                high = stack.pop()!
                low = stack.pop()!
            }
            
            if low < high {
                pivotIndex = Int.random(in: low...high)
                items.swapAt(pivotIndex, high)
                pivotIndex = high
            }
            i = low
            j = low
        }
        
        guard isSorted == false && pivotIndex != -1 else {
            return .failure(.isSorted)
        }
        
        let pivot = items[pivotIndex]
        
        if j < high {
            if items[j] <= pivot {
                items.swapAt(i, j)
                
                i += 1
                j += 1
                
                return .success((items: items, steps: totalSteps))
            } else {
                j += 1
                
                return .success((items: items, steps: totalSteps))
            }
        } else {
            items.swapAt(i, high)
            
            stack.push(i + 1)
            stack.push(high)
            
            stack.push(low)
            stack.push(i-1)
            
            pivotIndex = -1
            low = 0
            high = 0
            
            return .success((items: items, steps: totalSteps))
        }
    }
}
