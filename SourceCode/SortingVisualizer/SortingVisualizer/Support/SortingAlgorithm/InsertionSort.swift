import Foundation

class InsertionSort<T: Comparable>: SortingAlgorithm<T> {
    typealias Item = T
    
    // MARK: - Items
    
    private(set) var items: [Item]
    
    // MARK: - Variables
    
    private lazy var outerLoopIndex = 1
    private lazy var innerLoopIndex = outerLoopIndex
    
    private var totalSteps = 1
    
    // MARK: - API
     
    override var isSorted: Bool {
        outerLoopIndex >= items.count
    }
    
    required init(items: [T]) {
        self.items = items
        
        super.init(items: items)
    }
    
    override func next() -> Result<SuccessResult, SortingAlgorithmError> {
        defer {
            totalSteps += 1
        }
        
        guard isSorted == false else {
            return .failure(.isSorted)
        }
        
        guard innerLoopIndex > 0 && self.items[innerLoopIndex] < self.items[innerLoopIndex - 1] else {
            outerLoopIndex += 1
            innerLoopIndex = outerLoopIndex
            
            return .success((items: items, steps: totalSteps))
        }
        
        self.items.swapAt(innerLoopIndex, innerLoopIndex - 1)
        innerLoopIndex -= 1
        
        return .success((items: items, steps: totalSteps))
    }
}
