import Foundation

enum SortingType: Hashable {
    case insertionSort
    case quickSort
    
    var name: String {
        switch self {
        case .insertionSort:
            return "Insertion Sort"
            
        case .quickSort:
            return "Quick Sort"
        }
    }
}
