import Foundation

struct Item<T: Comparable & CustomStringConvertible>: Hashable, CustomStringConvertible, Comparable {

    let value: T
    
    init(value: T) {
        self.value = value
    }
    
    private let uuid = UUID()
    
    var description: String {
        return value.description
    }
    
    static func < (lhs: Item, rhs: Item) -> Bool {
        lhs.value < rhs.value
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(uuid)
    }
}
