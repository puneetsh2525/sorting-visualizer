import Foundation

class Section<T: Comparable>: Hashable {
    
    // MARK: - Private Properties
    
    private let sortingType: SortingType
    
    private let uuid = UUID()
    
    private lazy var sortingAlgorithm: SortingAlgorithm<T> = {
        switch sortingType {
        case .insertionSort:
            return InsertionSort(items: items)
            
        case .quickSort:
            return QuickSort(items: items)
        }
    }()
    
    // MARK: - API
    
    private(set) var items: [T]
    
    private(set) var totalSteps: Int = 0
    
    private(set) var isSorted = false
    
    var name: String {
        sortingType.name
    }
    
    func sortNext(_ completion: () -> Void) {
        let result = sortingAlgorithm.next()
        
        if case let .success(successResult) = result {
            self.items = successResult.items
            self.totalSteps = successResult.steps
            
            isSorted = false
        } else {
            isSorted = true
        }
        
        completion()
    }
    
    // MARK: - Init
    
    init(items: [T], sortingType: SortingType) {
        self.items = items
        self.sortingType = sortingType
    }

    // MARK: - Hashable and Equatable
    
    static func == (lhs: Section, rhs: Section) -> Bool {
        lhs.uuid == rhs.uuid
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(uuid)
    }
}
