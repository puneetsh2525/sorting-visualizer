![](sorting-visualizer.gif)

## Sorting Visualizer #

This repository is a simple one page iOS project to learn [UICollectionViewDiffableDatasource](https://developer.apple.com/documentation/uikit/uicollectionviewdiffabledatasource)

An array of integers are sorted step-by-step using different sorting algorithms, and then `UICollectionViewDiffableDatasource` is tasked to show the change in each step

### Sorting Algorithms 

- Insertion Sort 
- Quick Sort